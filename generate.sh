#!/bin/bash
set -e

PACKAGES="$(jq -r '.[]' < packages.json)"

for package in ${PACKAGES}; do
    source=$(apt-cache show "${package}" | grep '^Source' | awk '{print $2}')
    cifile="public/${source}.yml"
    cp ci-template.yml "${cifile}"
    for dep in $(build-rdeps --only-main -q "${package}"); do
        cat >> "${cifile}" << _END_

build-rdep-${dep}:
  variables:
    REVERSE_DEP: ${dep}
  extends: .rdep-build-definition

_END_

    done
done

